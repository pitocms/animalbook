<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AnimalCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AnimalCategoriesTable Test Case
 */
class AnimalCategoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AnimalCategoriesTable
     */
    public $AnimalCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.animal_categories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AnimalCategories') ? [] : ['className' => AnimalCategoriesTable::class];
        $this->AnimalCategories = TableRegistry::get('AnimalCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AnimalCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
