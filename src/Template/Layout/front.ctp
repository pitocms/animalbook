<?php
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="start.html">LOGO</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="col-sm-4"></div>
    <div class="collapse navbar-collapse" id="navbarSupportedContent" >
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Suchen" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Suchen</button>
        </form>

        <div class="col-sm-4"></div>

        <ul class="navbar-nav mr-auto" >
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Meine Tiere
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php foreach ($animalList as $key => $value) : ?>
                    <?= $this->Html->link($value->name,['controller'=>'Animals','action'=>'profile',$value->id],['class'=>'dropdown-item']); ?>
                    <?php endforeach; ?>
                </div>
                 
               
            </li>

            <li class="nav-item" style="padding-top:8px;padding-left:18px;">
                 <?= $this->Html->link('Logout',['controller'=>'Users','action'=>'logout']) ?>
            </li>
        </ul>
    </div>
</nav>

<div class="container" style="max-width: 1500px;">
    <div class="row">
    <!-- left sidebar     -->
    <div class="col-md-auto">
            <div class="col-sm-12"><br></div>
            <div class="col-sm-12"><br></div>
            <div class="card" style="width: 20rem;">
                <div class="list-group">
                    <?= $this->Html->link('Animal List',['controller'=>'Animals','action'=>'index'],['class'=>'list-group-item']); ?>
                    <?= $this->Html->link('New Animal Register',['controller'=>'Animals','action'=>'add'],['class'=>'list-group-item']); ?> 
                    <?= $this->Html->link('Animal Category',['controller'=>'AnimalCategories','action'=>'index'],['class'=>'list-group-item']); ?>  

                    <?= $this->Html->link('New Animal Category',['controller'=>'AnimalCategories','action'=>'add'],['class'=>'list-group-item']); ?> 

                </div>
            </div>
    </div>
    <!-- end left-sidebar -->

    <div class="col-sm-8">
    <?= $this->fetch('content') ?>
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <?= $this->Flash->render() ?>
                </div>
            </div>
        </div>
    </div>
    </div>

</div>
</div>

</body>
</html>
