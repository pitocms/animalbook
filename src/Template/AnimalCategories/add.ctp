
<div class="animalCategories form large-9 medium-8 columns content">
    <?= $this->Form->create($animalCategory) ?>
    <fieldset>
        <legend><?= __('Add Animal Category') ?></legend>
        <?php
            echo $this->Form->control('name',['class'=>'form-control','placeholder'=>'Category Name']);
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button(__('Submit'),['class'=>'btn btn-outline-primary my-2 my-sm-0','style'=>"float: right"]) ?>
    <?= $this->Form->end() ?>
</div>
