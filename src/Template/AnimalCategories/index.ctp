<div class="animals index large-9 medium-8 columns content">
    <h3><?= __('Animal Categories') ?></h3>
    <table class="table">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($animalCategories as $animalCategory): ?>
            <tr>
                <td><?= $this->Number->format($animalCategory->id) ?></td>
                <td><?= h($animalCategory->name) ?></td>
                <td><?= h($animalCategory->created) ?></td>
                <td><?= h($animalCategory->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $animalCategory->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $animalCategory->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $animalCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $animalCategory->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
