<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AnimalCategory $animalCategory
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $animalCategory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $animalCategory->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Animal Categories'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="animalCategories form large-9 medium-8 columns content">
    <?= $this->Form->create($animalCategory) ?>
    <fieldset>
        <legend><?= __('Edit Animal Category') ?></legend>
        <?php
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
