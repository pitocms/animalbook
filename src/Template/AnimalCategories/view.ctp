<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AnimalCategory $animalCategory
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Animal Category'), ['action' => 'edit', $animalCategory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Animal Category'), ['action' => 'delete', $animalCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $animalCategory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Animal Categories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Animal Category'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="animalCategories view large-9 medium-8 columns content">
    <h3><?= h($animalCategory->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($animalCategory->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($animalCategory->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($animalCategory->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($animalCategory->modified) ?></td>
        </tr>
    </table>
</div>
