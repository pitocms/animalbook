<div class="animals form large-9 medium-8 columns content">
    <?= $this->Form->create($animal,['type'=>'file']) ?>
    <fieldset>
        <legend><?= __('Animal register') ?></legend>
        <?php
            echo $this->Form->control('name',['class'=>'form-control','placeholder'=>'Name']);

            echo $this->Form->control('animal_category_id',['class'=>'form-control','placeholder'=>'Name','empty'=>'--Select One --']);

            echo $this->Form->control('email',['class'=>'form-control','placeholder'=>'Email']);
            echo $this->Form->control('date_of_birth',['type'=>'text','class'=>'form-control','id'=>'birthDate','placeholder'=>'YY-MM-DD']);
            echo $this->Form->control('address',['class'=>'form-control','placeholder'=>'Address..']);
        ?>
        <br>
        <?php 
            echo $this->Form->control('image',['type'=>'file','label'=>'Image:']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'),['class'=>'btn btn-outline-primary my-2 my-sm-0','style'=>"float: right"]) ?>
    <?= $this->Form->end() ?>
</div>
