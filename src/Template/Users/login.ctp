<div class="top-content" style="margin-top: 5%;">
    <div class="inner-bg">
        <div class="container">
            <div class="row">

                <div class="col-sm-5">

                    <div class="form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Sign up</h3>
                                <p>Füllen Sie Bitte auf:</p>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-pencil"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                        <?= $this->Form->create($user, ['url' => ['action' => 'add']]); ?>

                        <div class="form-group">
                            <?= $this->Form->control("username",['class'=>'form-control','required'=>'true','placeholder'=>'User Name','label'=>false]); ?>
                        </div>

                        <div class="form-group">
                            <?= $this->Form->control("email",['class'=>'form-control','required'=>'true','placeholder'=>'Your Email','label'=>false]); ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control("password",['class'=>'form-control form-password','required'=>'true','placeholder'=>'Your Password','label'=>false,'type'=>'password']); ?>
                        </div>

                        <div class="form-group">
                            <?= $this->Form->control("confirm_password",['class'=>'form-control form-password','required'=>'true','placeholder'=>'Confirm Password','label'=>false,'type'=>'password']); ?>
                        </div>

                        <button type="submit" class="btn btn-outline-primary my-2 my-sm-0" style="float: right">Sign up</button>

                        <?= $this->Form->end(); ?>
                        </div>
                    </div>

                </div>

                

                <div class="col-sm-1 middle-border"></div>
                <div class="col-sm-1"></div>
                
                <div class="col-sm-5">

                    <div class="form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Login to PetBook</h3>
                                <p>Geben Sie bitte Ihre Login und Password:</p>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <?= $this->Form->create('users'); ?>
                                <div class="form-group">
                                    <?= $this->Form->input("email",['class'=>'form-control','required'=>'true','placeholder'=>'Your Email','label'=>false]); ?>
                                </div>
                                <div class="form-group">
                                    <?= $this->Form->input("password",['class'=>'form-control form-password','required'=>'true','placeholder'=>'Your Password','label'=>false,'type'=>'password']); ?>
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">
                                    <a href="#"><h3>Password vergessen?</h3></a>
                                    </div>
                                    <div class="col-sm-4">
                                    <button type="submit" class="btn btn-outline-primary my-2 my-sm-0" style="float: right">Login</button>
                                    </div>
                                </div>
                            <?= $this->Form->end(); ?>
                        </div>

                        <?php //echo $this->Flash->render(); ?>
                    </div>
                </div>
                
            </div>

        </div>
    </div>
</div>