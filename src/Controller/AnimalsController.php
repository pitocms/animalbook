<?php
namespace App\Controller;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;


class AnimalsController extends AppController
{


    public function index()
    {
        $this->paginate = [
            'contain' => ['Users','AnimalCategories']
        ];
        $animals = $this->paginate($this->Animals);

        $this->set(compact('animals'));
    }

    public function view($id = null)
    {
        $animal = $this->Animals->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('animal', $animal);
    }


    public function add()
    {
        $addressTable   = TableRegistry::get('Address');
        $animal = $this->Animals->newEntity();
        if ($this->request->is('post')) {
            $animal = $this->Animals->patchEntity($animal, $this->request->getData());
            $animal->user_id = $this->Auth->user('id');
            // debug($this->request->data['image']['name']);
            // exit();

            if(!empty($this->request->data['image']['name'])){
                $image_name = date("Y-m-d-H-i-s").str_replace( " ", "-", $this->request->data['image']['name'] );
                $image_tmp         = $this->request->data['image']['tmp_name'];
                $destination       = WWW_ROOT.'img'.DS.'animals'.DS.$image_name;
                move_uploaded_file($image_tmp, $destination);
                $animal->image = 'animals/'.$image_name;
            }

            if ($this->Animals->save($animal)) {
                $address = $addressTable->newEntity();
                $address->animal_id = 1;
                $address->address   = $this->request->data('address');
                $addressTable->save($address);
                $this->Flash->success(__('The animal has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The animal could not be saved. Please, try again.'));
        }
        $users = $this->Animals->Users->find('list', ['limit' => 200]);
        $animalCategories = $this->Animals->AnimalCategories->find('list', ['limit' => 200]);
        $this->set(compact('animal', 'users','animalCategories'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Animal id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $animal = $this->Animals->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $animal = $this->Animals->patchEntity($animal, $this->request->getData());
            if ($this->Animals->save($animal)) {
                $this->Flash->success(__('The animal has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The animal could not be saved. Please, try again.'));
        }
        $users = $this->Animals->Users->find('list', ['limit' => 200]);
        $this->set(compact('animal', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Animal id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $animal = $this->Animals->get($id);
        if ($this->Animals->delete($animal)) {
            $this->Flash->success(__('The animal has been deleted.'));
        } else {
            $this->Flash->error(__('The animal could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
