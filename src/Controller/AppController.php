<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppController extends Controller
{


    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash', ['clear' => true]);

        $this->loadComponent( 'Auth', [
            'authenticate' => [
                'Form' => [
                   'fields' => [ 'username' => 'email' ]
                ]
            ],
            'loginRedirect' => [
                'controller' => 'animals',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login'
            ],
            //'authorize' => array( 'Controller' )
        ]);


        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');

        $this->loadModel('Animals');
        $animalList = $this->Animals->find('all',['conditions'=>['user_id'=>$this->Auth->user('id')]]);
        $this->set(compact('animalList'));
    }

    public function beforeFilter(Event $event)
    {
       $this->viewBuilder()->setLayout('front');
       $this->Auth->allow( [ 'add','login', 'logout','forgottenPassword','resetmail','reset' ] );
       $this->set('userAuth',$this->Auth->user());
    }
}
