<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AnimalCategories Controller
 *
 * @property \App\Model\Table\AnimalCategoriesTable $AnimalCategories
 *
 * @method \App\Model\Entity\AnimalCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AnimalCategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $animalCategories = $this->paginate($this->AnimalCategories);

        $this->set(compact('animalCategories'));
    }

    /**
     * View method
     *
     * @param string|null $id Animal Category id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $animalCategory = $this->AnimalCategories->get($id, [
            'contain' => []
        ]);

        $this->set('animalCategory', $animalCategory);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $animalCategory = $this->AnimalCategories->newEntity();
        if ($this->request->is('post')) {
            $animalCategory = $this->AnimalCategories->patchEntity($animalCategory, $this->request->getData());
            if ($this->AnimalCategories->save($animalCategory)) {
                $this->Flash->success(__('The animal category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The animal category could not be saved. Please, try again.'));
        }
        $this->set(compact('animalCategory'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Animal Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $animalCategory = $this->AnimalCategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $animalCategory = $this->AnimalCategories->patchEntity($animalCategory, $this->request->getData());
            if ($this->AnimalCategories->save($animalCategory)) {
                $this->Flash->success(__('The animal category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The animal category could not be saved. Please, try again.'));
        }
        $this->set(compact('animalCategory'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Animal Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $animalCategory = $this->AnimalCategories->get($id);
        if ($this->AnimalCategories->delete($animalCategory)) {
            $this->Flash->success(__('The animal category has been deleted.'));
        } else {
            $this->Flash->error(__('The animal category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
