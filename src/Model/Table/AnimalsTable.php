<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class AnimalsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('animals');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('AnimalCategories', [
            'foreignKey' => 'animal_category_id',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        // $validator
        //     ->scalar('name')
        //     ->maxLength('name', 50)
        //     ->requirePresence('name', 'create')
        //     ->notEmpty('name');

        // $validator
        //     ->email('email')
        //     ->requirePresence('email', 'create')
        //     ->notEmpty('email');

        // $validator
        //     ->date('date_of_birth')
        //     ->requirePresence('date_of_birth', 'create')
        //     ->notEmpty('date_of_birth');

        // $validator
        //     ->scalar('address')
        //     ->requirePresence('address', 'create')
        //     ->notEmpty('address');

        // $validator
        //     ->scalar('image')
        //     ->maxLength('image', 255)
        //     ->requirePresence('image', 'create')
        //     ->notEmpty('image');

        return $validator;
    }

    
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['animal_category_id'], 'AnimalCategories'));
        return $rules;
    }
}
