<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Animal extends Entity
{
    protected $_accessible = [
        'name' => true,
        'email' => true,
        'date_of_birth' => true,
        'address' => true,
        'image' => true,
        'user_id' => true,
        'animal_category_id'=>true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
