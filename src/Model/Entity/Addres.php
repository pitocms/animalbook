<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Addres Entity
 *
 * @property int $id
 * @property int $animal_id
 * @property string $address
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Animal $animal
 */
class Addres extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'animal_id' => true,
        'address' => true,
        'created' => true,
        'animal' => true
    ];
}
